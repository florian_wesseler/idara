class CreateServiceTurnovers < ActiveRecord::Migration
  def change
    create_table :service_turnovers do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :beschreibung
      t.datetime :belegdatum
      t.decimal :betrag
      t.string :waehrung
      t.boolean :rechnungsdruck
      t.integer :gevis_akzeptanzstelle

      t.timestamps null: false
    end
  end
end
