class PartnerRolesController < ApplicationController
  before_action :set_partner_role, only: [:show, :edit, :update, :destroy]

  # GET /partner_roles
  # GET /partner_roles.json
  def index
    @partner_roles = PartnerRole.all
  end

  # GET /partner_roles/1
  # GET /partner_roles/1.json
  def show
  end

  # GET /partner_roles/new
  def new
    @partner_role = PartnerRole.new
  end

  # GET /partner_roles/1/edit
  def edit
  end

  # POST /partner_roles
  # POST /partner_roles.json
  def create
    @partner_role = PartnerRole.new(partner_role_params)

    respond_to do |format|
      if @partner_role.save
        format.html { redirect_to @partner_role, notice: 'Partner role was successfully created.' }
        format.json { render :show, status: :created, location: @partner_role }
      else
        format.html { render :new }
        format.json { render json: @partner_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partner_roles/1
  # PATCH/PUT /partner_roles/1.json
  def update
    respond_to do |format|
      if @partner_role.update(partner_role_params)
        format.html { redirect_to @partner_role, notice: 'Partner role was successfully updated.' }
        format.json { render :show, status: :ok, location: @partner_role }
      else
        format.html { render :edit }
        format.json { render json: @partner_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partner_roles/1
  # DELETE /partner_roles/1.json
  def destroy
    @partner_role.destroy
    respond_to do |format|
      format.html { redirect_to partner_roles_url, notice: 'Partner role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner_role
      @partner_role = PartnerRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_role_params
      params.require(:partner_role).permit(:partner_id, :partnerrolle, :gueltig_von, :gueltig_bis)
    end
end
