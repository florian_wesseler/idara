class ServiceTurnoversController < ApplicationController
  before_action :set_service_turnover, only: [:show, :edit, :update, :destroy]

  # GET /service_turnovers
  # GET /service_turnovers.json
  def index
    @service_turnovers = ServiceTurnover.all
  end

  # GET /service_turnovers/1
  # GET /service_turnovers/1.json
  def show
  end

  # GET /service_turnovers/new
  def new
    @service_turnover = ServiceTurnover.new
  end

  # GET /service_turnovers/1/edit
  def edit
  end

  # POST /service_turnovers
  # POST /service_turnovers.json
  def create
    @service_turnover = ServiceTurnover.new(service_turnover_params)

    respond_to do |format|
      if @service_turnover.save
        format.html { redirect_to @service_turnover, notice: 'Service turnover was successfully created.' }
        format.json { render :show, status: :created, location: @service_turnover }
      else
        format.html { render :new }
        format.json { render json: @service_turnover.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_turnovers/1
  # PATCH/PUT /service_turnovers/1.json
  def update
    respond_to do |format|
      if @service_turnover.update(service_turnover_params)
        format.html { redirect_to @service_turnover, notice: 'Service turnover was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_turnover }
      else
        format.html { render :edit }
        format.json { render json: @service_turnover.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_turnovers/1
  # DELETE /service_turnovers/1.json
  def destroy
    @service_turnover.destroy
    respond_to do |format|
      format.html { redirect_to service_turnovers_url, notice: 'Service turnover was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_turnover
      @service_turnover = ServiceTurnover.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_turnover_params
      params.require(:service_turnover).permit(:partner_id, :beschreibung, :belegdatum, :betrag, :waehrung, :rechnungsdruck, :gevis_akzeptanzstelle)
    end
end
