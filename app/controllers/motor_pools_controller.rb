class MotorPoolsController < ApplicationController
  before_action :set_motor_pool, only: [:show, :edit, :update, :destroy]

  # GET /motor_pools
  # GET /motor_pools.json
  def index
    @motor_pools = MotorPool.all
  end

  # GET /motor_pools/1
  # GET /motor_pools/1.json
  def show
  end

  # GET /motor_pools/new
  def new
    @motor_pool = MotorPool.new
  end

  # GET /motor_pools/1/edit
  def edit
  end

  # POST /motor_pools
  # POST /motor_pools.json
  def create
    @motor_pool = MotorPool.new(motor_pool_params)

    respond_to do |format|
      if @motor_pool.save
        format.html { redirect_to @motor_pool, notice: 'Motor pool was successfully created.' }
        format.json { render :show, status: :created, location: @motor_pool }
      else
        format.html { render :new }
        format.json { render json: @motor_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /motor_pools/1
  # PATCH/PUT /motor_pools/1.json
  def update
    respond_to do |format|
      if @motor_pool.update(motor_pool_params)
        format.html { redirect_to @motor_pool, notice: 'Motor pool was successfully updated.' }
        format.json { render :show, status: :ok, location: @motor_pool }
      else
        format.html { render :edit }
        format.json { render json: @motor_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /motor_pools/1
  # DELETE /motor_pools/1.json
  def destroy
    @motor_pool.destroy
    respond_to do |format|
      format.html { redirect_to motor_pools_url, notice: 'Motor pool was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_motor_pool
      @motor_pool = MotorPool.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def motor_pool_params
      params.require(:motor_pool).permit(:partner_id, :anzahl_pkw_kl_3_5, :anzahl_pkw_gr_3_5, :anzahl_lkw_gr_12, :anzahl_busse, :tankstellen_privat, :mautabrechner, :hauptkartenabrechner, :erfassungsdatum)
    end
end
