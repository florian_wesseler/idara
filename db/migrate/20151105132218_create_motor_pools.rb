class CreateMotorPools < ActiveRecord::Migration
  def change
    create_table :motor_pools do |t|
      t.references :partner, index: true, foreign_key: true
      t.integer :anzahl_pkw_kl_3_5
      t.integer :anzahl_pkw_gr_3_5
      t.integer :anzahl_lkw_gr_12
      t.integer :anzahl_busse
      t.string :tankstellen_privat
      t.string :mautabrechner
      t.string :hauptkartenabrechner
      t.datetime :erfassungsdatum

      t.timestamps null: false
    end
  end
end
