class FuelTurnoversController < ApplicationController
  before_action :set_fuel_turnover, only: [:show, :edit, :update, :destroy]

  # GET /fuel_turnovers
  # GET /fuel_turnovers.json
  def index
    @fuel_turnovers = FuelTurnover.all
  end

  # GET /fuel_turnovers/1
  # GET /fuel_turnovers/1.json
  def show
  end

  # GET /fuel_turnovers/new
  def new
    @fuel_turnover = FuelTurnover.new
  end

  # GET /fuel_turnovers/1/edit
  def edit
  end

  # POST /fuel_turnovers
  # POST /fuel_turnovers.json
  def create
    @fuel_turnover = FuelTurnover.new(fuel_turnover_params)

    respond_to do |format|
      if @fuel_turnover.save
        format.html { redirect_to @fuel_turnover, notice: 'Fuel turnover was successfully created.' }
        format.json { render :show, status: :created, location: @fuel_turnover }
      else
        format.html { render :new }
        format.json { render json: @fuel_turnover.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fuel_turnovers/1
  # PATCH/PUT /fuel_turnovers/1.json
  def update
    respond_to do |format|
      if @fuel_turnover.update(fuel_turnover_params)
        format.html { redirect_to @fuel_turnover, notice: 'Fuel turnover was successfully updated.' }
        format.json { render :show, status: :ok, location: @fuel_turnover }
      else
        format.html { render :edit }
        format.json { render json: @fuel_turnover.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fuel_turnovers/1
  # DELETE /fuel_turnovers/1.json
  def destroy
    @fuel_turnover.destroy
    respond_to do |format|
      format.html { redirect_to fuel_turnovers_url, notice: 'Fuel turnover was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fuel_turnover
      @fuel_turnover = FuelTurnover.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fuel_turnover_params
      params.require(:fuel_turnover).permit(:dkv_nummer, :partner_id, :beschreibung, :belegdatum, :betrag, :waehrung, :rechnungsdruck)
    end
end
