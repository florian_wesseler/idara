require 'test_helper'

class PemaRentsControllerTest < ActionController::TestCase
  setup do
    @pema_rent = pema_rents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pema_rents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pema_rent" do
    assert_difference('PemaRent.count') do
      post :create, pema_rent: { fahrgestellnummer: @pema_rent.fahrgestellnummer, kaution: @pema_rent.kaution, laufzeit: @pema_rent.laufzeit, partner_id: @pema_rent.partner_id, status: @pema_rent.status, vertragsbeginn: @pema_rent.vertragsbeginn, vertragsende: @pema_rent.vertragsende, vertragsnummer: @pema_rent.vertragsnummer, zusatzinfos: @pema_rent.zusatzinfos }
    end

    assert_redirected_to pema_rent_path(assigns(:pema_rent))
  end

  test "should show pema_rent" do
    get :show, id: @pema_rent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pema_rent
    assert_response :success
  end

  test "should update pema_rent" do
    patch :update, id: @pema_rent, pema_rent: { fahrgestellnummer: @pema_rent.fahrgestellnummer, kaution: @pema_rent.kaution, laufzeit: @pema_rent.laufzeit, partner_id: @pema_rent.partner_id, status: @pema_rent.status, vertragsbeginn: @pema_rent.vertragsbeginn, vertragsende: @pema_rent.vertragsende, vertragsnummer: @pema_rent.vertragsnummer, zusatzinfos: @pema_rent.zusatzinfos }
    assert_redirected_to pema_rent_path(assigns(:pema_rent))
  end

  test "should destroy pema_rent" do
    assert_difference('PemaRent.count', -1) do
      delete :destroy, id: @pema_rent
    end

    assert_redirected_to pema_rents_path
  end
end
