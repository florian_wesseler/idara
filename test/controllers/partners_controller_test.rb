require 'test_helper'

class PartnersControllerTest < ActionController::TestCase
  setup do
    @partner = partners(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:partners)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create partner" do
    assert_difference('Partner.count') do
      post :create, partner: { abteilung: @partner.abteilung, bemerkungen: @partner.bemerkungen, dkv_nummer: @partner.dkv_nummer, handelsregister_nr: @partner.handelsregister_nr, handelsregister_ort: @partner.handelsregister_ort, info_interessent: @partner.info_interessent, kunde_seit: @partner.kunde_seit, ort_postfach: @partner.ort_postfach, partner_typ: @partner.partner_typ, plz_postfach: @partner.plz_postfach, postfach: @partner.postfach, rating_bemerkung: @partner.rating_bemerkung }
    end

    assert_redirected_to partner_path(assigns(:partner))
  end

  test "should show partner" do
    get :show, id: @partner
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @partner
    assert_response :success
  end

  test "should update partner" do
    patch :update, id: @partner, partner: { abteilung: @partner.abteilung, bemerkungen: @partner.bemerkungen, dkv_nummer: @partner.dkv_nummer, handelsregister_nr: @partner.handelsregister_nr, handelsregister_ort: @partner.handelsregister_ort, info_interessent: @partner.info_interessent, kunde_seit: @partner.kunde_seit, ort_postfach: @partner.ort_postfach, partner_typ: @partner.partner_typ, plz_postfach: @partner.plz_postfach, postfach: @partner.postfach, rating_bemerkung: @partner.rating_bemerkung }
    assert_redirected_to partner_path(assigns(:partner))
  end

  test "should destroy partner" do
    assert_difference('Partner.count', -1) do
      delete :destroy, id: @partner
    end

    assert_redirected_to partners_path
  end
end
