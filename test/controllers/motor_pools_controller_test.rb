require 'test_helper'

class MotorPoolsControllerTest < ActionController::TestCase
  setup do
    @motor_pool = motor_pools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:motor_pools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create motor_pool" do
    assert_difference('MotorPool.count') do
      post :create, motor_pool: { anzahl_busse: @motor_pool.anzahl_busse, anzahl_lkw_gr_12: @motor_pool.anzahl_lkw_gr_12, anzahl_pkw_gr_3_5: @motor_pool.anzahl_pkw_gr_3_5, anzahl_pkw_kl_3_5: @motor_pool.anzahl_pkw_kl_3_5, erfassungsdatum: @motor_pool.erfassungsdatum, hauptkartenabrechner: @motor_pool.hauptkartenabrechner, mautabrechner: @motor_pool.mautabrechner, partner_id: @motor_pool.partner_id, tankstellen_privat: @motor_pool.tankstellen_privat }
    end

    assert_redirected_to motor_pool_path(assigns(:motor_pool))
  end

  test "should show motor_pool" do
    get :show, id: @motor_pool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @motor_pool
    assert_response :success
  end

  test "should update motor_pool" do
    patch :update, id: @motor_pool, motor_pool: { anzahl_busse: @motor_pool.anzahl_busse, anzahl_lkw_gr_12: @motor_pool.anzahl_lkw_gr_12, anzahl_pkw_gr_3_5: @motor_pool.anzahl_pkw_gr_3_5, anzahl_pkw_kl_3_5: @motor_pool.anzahl_pkw_kl_3_5, erfassungsdatum: @motor_pool.erfassungsdatum, hauptkartenabrechner: @motor_pool.hauptkartenabrechner, mautabrechner: @motor_pool.mautabrechner, partner_id: @motor_pool.partner_id, tankstellen_privat: @motor_pool.tankstellen_privat }
    assert_redirected_to motor_pool_path(assigns(:motor_pool))
  end

  test "should destroy motor_pool" do
    assert_difference('MotorPool.count', -1) do
      delete :destroy, id: @motor_pool
    end

    assert_redirected_to motor_pools_path
  end
end
