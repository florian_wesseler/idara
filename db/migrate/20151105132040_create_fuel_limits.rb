class CreateFuelLimits < ActiveRecord::Migration
  def change
    create_table :fuel_limits do |t|
      t.references :partner, index: true, foreign_key: true
      t.integer :dkv_nummer
      t.decimal :limit
      t.datetime :limit_seit
      t.datetime :limit_bis
      t.datetime :haftungszusage_dkv_seit

      t.timestamps null: false
    end
  end
end
