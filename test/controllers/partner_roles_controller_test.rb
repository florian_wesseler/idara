require 'test_helper'

class PartnerRolesControllerTest < ActionController::TestCase
  setup do
    @partner_role = partner_roles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:partner_roles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create partner_role" do
    assert_difference('PartnerRole.count') do
      post :create, partner_role: { gueltig_bis: @partner_role.gueltig_bis, gueltig_von: @partner_role.gueltig_von, partner_id: @partner_role.partner_id, partnerrolle: @partner_role.partnerrolle }
    end

    assert_redirected_to partner_role_path(assigns(:partner_role))
  end

  test "should show partner_role" do
    get :show, id: @partner_role
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @partner_role
    assert_response :success
  end

  test "should update partner_role" do
    patch :update, id: @partner_role, partner_role: { gueltig_bis: @partner_role.gueltig_bis, gueltig_von: @partner_role.gueltig_von, partner_id: @partner_role.partner_id, partnerrolle: @partner_role.partnerrolle }
    assert_redirected_to partner_role_path(assigns(:partner_role))
  end

  test "should destroy partner_role" do
    assert_difference('PartnerRole.count', -1) do
      delete :destroy, id: @partner_role
    end

    assert_redirected_to partner_roles_path
  end
end
