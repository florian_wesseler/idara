json.array!(@terminations) do |termination|
  json.extract! termination, :id, :partner_id, :variante, :sofort, :terminiert, :status
  json.url termination_url(termination, format: :json)
end
