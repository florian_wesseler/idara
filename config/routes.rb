Rails.application.routes.draw do



  resources :cards
  resources :turnovers

  get 'welcome/index'

  match 'kundenuebersicht_part', to: "partners#kundenuebersicht", via: :all
  match 'limite_part', to: "partners#limite", via: :all

  resources :validations
  resources :terminations
  resources :service_turnovers
  resources :service_limits
  resources :sales_informations
  resources :return_debit_notes
  resources :pema_rents
  resources :payment_troubles
  resources :partner_roles
  resources :partners
  resources :motor_pools
  resources :liability_requests
  resources :invoices
  resources :fuel_turnovers
  resources :fuel_limits
  resources :fuel_cards
  resources :contacts
  resources :card_contracts

  resources :partners do
    resources :invoices
  end

  # resources :partners do
  #   resources :fuel_limits
  # end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
