# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151110105852) do

  create_table "card_contracts", force: :cascade do |t|
    t.integer  "partner_id",     precision: 38
    t.string   "kundentyp"
    t.integer  "vertragsnummer", precision: 38
    t.datetime "vertrag_datum"
    t.string   "vermerk"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "card_contracts", ["partner_id"], name: "i_card_contracts_partner_id"

  create_table "contacts", force: :cascade do |t|
    t.integer  "partner_id",                 precision: 38
    t.string   "ansprechpartner_vorname"
    t.string   "ansprechpartner_name"
    t.string   "ansprechpartner_titel"
    t.string   "ansprechpartner_abteilung"
    t.string   "ansprechpartner_geschlecht"
    t.string   "ansprechpartner_funktion"
    t.string   "ansprechpartner_telefon"
    t.string   "ansprechpartner_fax"
    t.string   "ansprechpartner_email"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "contacts", ["partner_id"], name: "index_contacts_on_partner_id"

  create_table "fuel_cards", force: :cascade do |t|
    t.integer  "partner_id",   precision: 38
    t.integer  "dkv_nummer",   precision: 38
    t.integer  "anzahl_",      precision: 38
    t.datetime "gueltig_seit"
    t.datetime "gueltig_bis"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "fuel_cards", ["partner_id"], name: "index_fuel_cards_on_partner_id"

  create_table "fuel_limits", force: :cascade do |t|
    t.integer  "partner_id",              precision: 38
    t.integer  "dkv_nummer",              precision: 38
    t.integer  "limit",                   precision: 38
    t.datetime "limit_seit"
    t.datetime "limit_bis"
    t.datetime "haftungszusage_dkv_seit"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "fuel_limits", ["partner_id"], name: "i_fuel_limits_partner_id"

  create_table "fuel_turnovers", force: :cascade do |t|
    t.integer  "dkv_nummer",                 precision: 38
    t.integer  "partner_id",                 precision: 38
    t.string   "beschreibung"
    t.datetime "belegdatum"
    t.integer  "betrag",                     precision: 38
    t.string   "waehrung"
    t.boolean  "rechnungsdruck", limit: nil
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "fuel_turnovers", ["partner_id"], name: "i_fuel_turnovers_partner_id"

  create_table "invoices", force: :cascade do |t|
    t.integer  "partner_id",     precision: 38
    t.string   "firma1"
    t.string   "vorname"
    t.string   "nachname"
    t.string   "firma2"
    t.string   "abteilung"
    t.string   "titel"
    t.string   "geschlecht"
    t.string   "funktion"
    t.string   "strasse"
    t.string   "hausnummer"
    t.integer  "plz",            precision: 38
    t.string   "ort"
    t.string   "postfach"
    t.integer  "postfach_plz",   precision: 38
    t.string   "postfach_ort"
    t.string   "lastschriftart"
    t.string   "zahlungsart"
    t.integer  "zahlungsziel",   precision: 38
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "invoices", ["partner_id"], name: "index_invoices_on_partner_id"

  create_table "liability_requests", force: :cascade do |t|
    t.integer  "partner_id",                   precision: 38
    t.datetime "anfrage_dkv"
    t.integer  "dkv_betrag",                   precision: 38
    t.string   "anfrage_verschickt_an"
    t.datetime "anfrage_verschickt_datum"
    t.string   "ergebnis_dkv_haftungsanfrage"
    t.integer  "anzahl_karten",                precision: 38
    t.string   "mitarbeiter_ad"
    t.datetime "mitarbeiter_ad_datum"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "liability_requests", ["partner_id"], name: "i_lia_req_par_id"

  create_table "motor_pools", force: :cascade do |t|
    t.integer  "partner_id",           precision: 38
    t.integer  "anzahl_pkw_kl_3_5",    precision: 38
    t.integer  "anzahl_pkw_gr_3_5",    precision: 38
    t.integer  "anzahl_lkw_gr_12",     precision: 38
    t.integer  "anzahl_busse",         precision: 38
    t.string   "tankstellen_privat"
    t.string   "mautabrechner"
    t.string   "hauptkartenabrechner"
    t.datetime "erfassungsdatum"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "motor_pools", ["partner_id"], name: "i_motor_pools_partner_id"

  create_table "partner_roles", force: :cascade do |t|
    t.integer  "partner_id",   precision: 38
    t.string   "partnerrolle"
    t.datetime "gueltig_von"
    t.datetime "gueltig_bis"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "partner_roles", ["partner_id"], name: "i_partner_roles_partner_id"

  create_table "partners", force: :cascade do |t|
    t.string   "partner_typ"
    t.integer  "dkv_nummer",          precision: 38
    t.string   "bemerkungen"
    t.string   "abteilung"
    t.integer  "plz_postfach",        precision: 38
    t.string   "ort_postfach"
    t.string   "postfach"
    t.string   "handelsregister_ort"
    t.integer  "handelsregister_nr",  precision: 38
    t.datetime "kunde_seit"
    t.string   "info_interessent"
    t.string   "rating_bemerkung"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "gevis_nummer",        precision: 38, null: false
  end

  create_table "payment_troubles", force: :cascade do |t|
    t.integer  "partner_id",       precision: 38
    t.string   "watchlist"
    t.datetime "watchlist_datum"
    t.integer  "mahnstufe",        precision: 38
    t.datetime "mahnstufe_datum"
    t.string   "sperrgrund"
    t.datetime "sperrgrund_datum"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "payment_troubles", ["partner_id"], name: "i_payment_troubles_partner_id"

  create_table "pema_rents", force: :cascade do |t|
    t.integer  "partner_id",        precision: 38
    t.integer  "vertragsnummer",    precision: 38
    t.string   "fahrgestellnummer"
    t.integer  "laufzeit",          precision: 38
    t.integer  "kaution",           precision: 38
    t.string   "status"
    t.datetime "vertragsbeginn"
    t.datetime "vertragsende"
    t.string   "zusatzinfos"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "pema_rents", ["partner_id"], name: "index_pema_rents_on_partner_id"

  create_table "return_debit_notes", force: :cascade do |t|
    t.integer  "partner_id",             precision: 38
    t.datetime "eingang_rls"
    t.string   "grund"
    t.datetime "faellig"
    t.integer  "betrag",                 precision: 38
    t.datetime "ausgleich_rls"
    t.integer  "rls_gebuehren",          precision: 38
    t.string   "tank_geschaeft"
    t.datetime "dkv_informiert"
    t.datetime "zahlungserinnerung"
    t.datetime "mahnung1"
    t.datetime "mahnung2"
    t.datetime "zahlungseingang_datum"
    t.integer  "zahlungseingang_betrag", precision: 38
    t.datetime "dkv_informiert2"
    t.datetime "gueltig_bis"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "return_debit_notes", ["partner_id"], name: "i_ret_deb_not_par_id"

  create_table "sales_informations", force: :cascade do |t|
    t.integer  "partner_id",      precision: 38
    t.string   "berater_vorname"
    t.string   "berater_name"
    t.string   "kompetenzcenter"
    t.string   "niederlassung"
    t.datetime "gueltig_bis"
    t.datetime "gueltig_von"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "sales_informations", ["partner_id"], name: "i_sal_inf_par_id"

  create_table "service_limits", force: :cascade do |t|
    t.integer  "partner_id",   precision: 38
    t.integer  "limit",        precision: 38
    t.datetime "limit_seit"
    t.datetime "limit_bis"
    t.datetime "service_seit"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "service_limits", ["partner_id"], name: "i_service_limits_partner_id"

  create_table "service_turnovers", force: :cascade do |t|
    t.integer  "partner_id",                        precision: 38
    t.string   "beschreibung"
    t.datetime "belegdatum"
    t.integer  "betrag",                            precision: 38
    t.string   "waehrung"
    t.boolean  "rechnungsdruck",        limit: nil
    t.integer  "gevis_akzeptanzstelle",             precision: 38
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  add_index "service_turnovers", ["partner_id"], name: "i_service_turnovers_partner_id"

  create_table "terminations", force: :cascade do |t|
    t.integer  "partner_id", precision: 38
    t.integer  "variante",   precision: 38
    t.string   "sofort"
    t.datetime "terminiert"
    t.string   "status"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "terminations", ["partner_id"], name: "i_terminations_partner_id"

  create_table "validations", force: :cascade do |t|
    t.integer  "partner_id", precision: 38
    t.integer  "betrag",     precision: 38
    t.string   "art"
    t.datetime "seit"
    t.datetime "ende"
    t.string   "bemerkung"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "validations", ["partner_id"], name: "i_validations_partner_id"

  add_foreign_key "card_contracts", "partners"
  add_foreign_key "contacts", "partners"
  add_foreign_key "fuel_cards", "partners"
  add_foreign_key "fuel_limits", "partners"
  add_foreign_key "fuel_turnovers", "partners"
  add_foreign_key "invoices", "partners"
  add_foreign_key "liability_requests", "partners"
  add_foreign_key "motor_pools", "partners"
  add_foreign_key "partner_roles", "partners"
  add_foreign_key "payment_troubles", "partners"
  add_foreign_key "pema_rents", "partners"
  add_foreign_key "return_debit_notes", "partners"
  add_foreign_key "sales_informations", "partners"
  add_foreign_key "service_limits", "partners"
  add_foreign_key "service_turnovers", "partners"
  add_foreign_key "terminations", "partners"
  add_foreign_key "validations", "partners"
end
