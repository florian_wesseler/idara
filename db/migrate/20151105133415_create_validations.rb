class CreateValidations < ActiveRecord::Migration
  def change
    create_table :validations do |t|
      t.references :partner, index: true, foreign_key: true
      t.decimal :betrag
      t.string :art
      t.datetime :seit
      t.datetime :ende
      t.string :bemerkung

      t.timestamps null: false
    end
  end
end
