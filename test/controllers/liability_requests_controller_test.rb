require 'test_helper'

class LiabilityRequestsControllerTest < ActionController::TestCase
  setup do
    @liability_request = liability_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:liability_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create liability_request" do
    assert_difference('LiabilityRequest.count') do
      post :create, liability_request: { anfrage_dkv: @liability_request.anfrage_dkv, anfrage_verschickt_an: @liability_request.anfrage_verschickt_an, anfrage_verschickt_datum: @liability_request.anfrage_verschickt_datum, anzahl_karten: @liability_request.anzahl_karten, dkv_betrag: @liability_request.dkv_betrag, ergebnis_dkv_haftungsanfrage: @liability_request.ergebnis_dkv_haftungsanfrage, mitarbeiter_ad: @liability_request.mitarbeiter_ad, mitarbeiter_ad_datum: @liability_request.mitarbeiter_ad_datum, partner_id: @liability_request.partner_id }
    end

    assert_redirected_to liability_request_path(assigns(:liability_request))
  end

  test "should show liability_request" do
    get :show, id: @liability_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @liability_request
    assert_response :success
  end

  test "should update liability_request" do
    patch :update, id: @liability_request, liability_request: { anfrage_dkv: @liability_request.anfrage_dkv, anfrage_verschickt_an: @liability_request.anfrage_verschickt_an, anfrage_verschickt_datum: @liability_request.anfrage_verschickt_datum, anzahl_karten: @liability_request.anzahl_karten, dkv_betrag: @liability_request.dkv_betrag, ergebnis_dkv_haftungsanfrage: @liability_request.ergebnis_dkv_haftungsanfrage, mitarbeiter_ad: @liability_request.mitarbeiter_ad, mitarbeiter_ad_datum: @liability_request.mitarbeiter_ad_datum, partner_id: @liability_request.partner_id }
    assert_redirected_to liability_request_path(assigns(:liability_request))
  end

  test "should destroy liability_request" do
    assert_difference('LiabilityRequest.count', -1) do
      delete :destroy, id: @liability_request
    end

    assert_redirected_to liability_requests_path
  end
end
