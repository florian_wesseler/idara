class CreatePemaRents < ActiveRecord::Migration
  def change
    create_table :pema_rents do |t|
      t.references :partner, index: true, foreign_key: true
      t.integer :vertragsnummer
      t.string :fahrgestellnummer
      t.integer :laufzeit
      t.decimal :kaution
      t.string :status
      t.datetime :vertragsbeginn
      t.datetime :vertragsende
      t.string :zusatzinfos

      t.timestamps null: false
    end
  end
end
