json.array!(@fuel_turnovers) do |fuel_turnover|
  json.extract! fuel_turnover, :id, :dkv_nummer, :partner_id, :beschreibung, :belegdatum, :betrag, :waehrung, :rechnungsdruck
  json.url fuel_turnover_url(fuel_turnover, format: :json)
end
