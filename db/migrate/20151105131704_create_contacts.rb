class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :ansprechpartner_vorname
      t.string :ansprechpartner_name
      t.string :ansprechpartner_titel
      t.string :ansprechpartner_abteilung
      t.string :ansprechpartner_geschlecht
      t.string :ansprechpartner_funktion
      t.string :ansprechpartner_telefon
      t.string :ansprechpartner_fax
      t.string :ansprechpartner_email

      t.timestamps null: false
    end
  end
end
