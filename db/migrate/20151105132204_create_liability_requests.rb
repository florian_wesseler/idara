class CreateLiabilityRequests < ActiveRecord::Migration
  def change
    create_table :liability_requests do |t|
      t.references :partner, index: true, foreign_key: true
      t.datetime :anfrage_dkv
      t.decimal :dkv_betrag
      t.string :anfrage_verschickt_an
      t.datetime :anfrage_verschickt_datum
      t.string :ergebnis_dkv_haftungsanfrage
      t.integer :anzahl_karten
      t.string :mitarbeiter_ad
      t.datetime :mitarbeiter_ad_datum

      t.timestamps null: false
    end
  end
end
