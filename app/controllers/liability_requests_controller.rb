class LiabilityRequestsController < ApplicationController
  before_action :set_liability_request, only: [:show, :edit, :update, :destroy]

  # GET /liability_requests
  # GET /liability_requests.json
  def index
    @liability_requests = LiabilityRequest.all
  end

  # GET /liability_requests/1
  # GET /liability_requests/1.json
  def show
  end

  # GET /liability_requests/new
  def new
    @liability_request = LiabilityRequest.new
  end

  # GET /liability_requests/1/edit
  def edit
  end

  # POST /liability_requests
  # POST /liability_requests.json
  def create
    @liability_request = LiabilityRequest.new(liability_request_params)

    respond_to do |format|
      if @liability_request.save
        format.html { redirect_to @liability_request, notice: 'Liability request was successfully created.' }
        format.json { render :show, status: :created, location: @liability_request }
      else
        format.html { render :new }
        format.json { render json: @liability_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /liability_requests/1
  # PATCH/PUT /liability_requests/1.json
  def update
    respond_to do |format|
      if @liability_request.update(liability_request_params)
        format.html { redirect_to @liability_request, notice: 'Liability request was successfully updated.' }
        format.json { render :show, status: :ok, location: @liability_request }
      else
        format.html { render :edit }
        format.json { render json: @liability_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /liability_requests/1
  # DELETE /liability_requests/1.json
  def destroy
    @liability_request.destroy
    respond_to do |format|
      format.html { redirect_to liability_requests_url, notice: 'Liability request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_liability_request
      @liability_request = LiabilityRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def liability_request_params
      params.require(:liability_request).permit(:partner_id, :anfrage_dkv, :dkv_betrag, :anfrage_verschickt_an, :anfrage_verschickt_datum, :ergebnis_dkv_haftungsanfrage, :anzahl_karten, :mitarbeiter_ad, :mitarbeiter_ad_datum)
    end
end
