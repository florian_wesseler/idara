json.array!(@validations) do |validation|
  json.extract! validation, :id, :partner_id, :betrag, :art, :seit, :ende, :bemerkung
  json.url validation_url(validation, format: :json)
end
