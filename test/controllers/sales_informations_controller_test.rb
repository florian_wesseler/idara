require 'test_helper'

class SalesInformationsControllerTest < ActionController::TestCase
  setup do
    @sales_information = sales_informations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_informations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_information" do
    assert_difference('SalesInformation.count') do
      post :create, sales_information: { berater_name: @sales_information.berater_name, berater_vorname: @sales_information.berater_vorname, gueltig_bis: @sales_information.gueltig_bis, gueltig_von: @sales_information.gueltig_von, kompetenzcenter: @sales_information.kompetenzcenter, niederlassung: @sales_information.niederlassung, partner_id: @sales_information.partner_id }
    end

    assert_redirected_to sales_information_path(assigns(:sales_information))
  end

  test "should show sales_information" do
    get :show, id: @sales_information
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_information
    assert_response :success
  end

  test "should update sales_information" do
    patch :update, id: @sales_information, sales_information: { berater_name: @sales_information.berater_name, berater_vorname: @sales_information.berater_vorname, gueltig_bis: @sales_information.gueltig_bis, gueltig_von: @sales_information.gueltig_von, kompetenzcenter: @sales_information.kompetenzcenter, niederlassung: @sales_information.niederlassung, partner_id: @sales_information.partner_id }
    assert_redirected_to sales_information_path(assigns(:sales_information))
  end

  test "should destroy sales_information" do
    assert_difference('SalesInformation.count', -1) do
      delete :destroy, id: @sales_information
    end

    assert_redirected_to sales_informations_path
  end
end
