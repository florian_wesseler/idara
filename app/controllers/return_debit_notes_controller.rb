class ReturnDebitNotesController < ApplicationController
  before_action :set_return_debit_note, only: [:show, :edit, :update, :destroy]

  # GET /return_debit_notes
  # GET /return_debit_notes.json
  def index
    @return_debit_notes = ReturnDebitNote.all
  end

  # GET /return_debit_notes/1
  # GET /return_debit_notes/1.json
  def show
  end

  # GET /return_debit_notes/new
  def new
    @return_debit_note = ReturnDebitNote.new
  end

  # GET /return_debit_notes/1/edit
  def edit
  end

  # POST /return_debit_notes
  # POST /return_debit_notes.json
  def create
    @return_debit_note = ReturnDebitNote.new(return_debit_note_params)

    respond_to do |format|
      if @return_debit_note.save
        format.html { redirect_to @return_debit_note, notice: 'Return debit note was successfully created.' }
        format.json { render :show, status: :created, location: @return_debit_note }
      else
        format.html { render :new }
        format.json { render json: @return_debit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /return_debit_notes/1
  # PATCH/PUT /return_debit_notes/1.json
  def update
    respond_to do |format|
      if @return_debit_note.update(return_debit_note_params)
        format.html { redirect_to @return_debit_note, notice: 'Return debit note was successfully updated.' }
        format.json { render :show, status: :ok, location: @return_debit_note }
      else
        format.html { render :edit }
        format.json { render json: @return_debit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /return_debit_notes/1
  # DELETE /return_debit_notes/1.json
  def destroy
    @return_debit_note.destroy
    respond_to do |format|
      format.html { redirect_to return_debit_notes_url, notice: 'Return debit note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_return_debit_note
      @return_debit_note = ReturnDebitNote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def return_debit_note_params
      params.require(:return_debit_note).permit(:partner_id, :eingang_rls, :grund, :faellig, :betrag, :ausgleich_rls, :rls_gebuehren, :tank_geschaeft, :dkv_informiert, :zahlungserinnerung, :mahnung1, :mahnung2, :zahlungseingang_datum, :zahlungseingang_betrag, :dkv_informiert2, :gueltig_bis)
    end
end
