class PartnersController < ApplicationController
  before_action :set_partner, only: [:show, :edit, :update, :destroy]

  # GET /partners
  # GET /partners.json
  def index
    @partners = Partner.all
  end

  # GET /partners/1
  # GET /partners/1.json
  def show

  end

  # GET /partners/new
  def new
    @partner = Partner.new
    @partner.build_invoice
    @partner.partner_roles.build
    @partner.build_contact
  end

  # GET /partners/1/edit
  def edit
  end

  # POST /partners
  # POST /partners.json
  def create
    @partner = Partner.new(partner_params)

    respond_to do |format|
      if @partner.save
        format.html { redirect_to @partner, notice: 'Partner was successfully created.' }
        format.json { render :show, status: :created, location: @partner }
      else
        format.html { render :new }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partners/1
  # PATCH/PUT /partners/1.json
  def update
    respond_to do |format|
      if @partner.update(partner_params)
        format.html { redirect_to @partner, notice: 'Partner was successfully updated.' }
        format.json { render :show, status: :ok, location: @partner }
      else
        format.html { render :edit }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partners/1
  # DELETE /partners/1.json
  def destroy
    @partner.destroy
    respond_to do |format|
      format.html { redirect_to partners_url, notice: 'Partner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def kundenuebersicht
    respond_to do |format|
      format.js
    end
  end

  def limite
    respond_to do |format|
      format.js
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner
      @partner = Partner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_params
      params.require(:partner).permit(:partner_typ, :dkv_nummer, :bemerkungen, :abteilung, :plz_postfach, :ort_postfach, :postfach, :handelsregister_ort, :handelsregister_nr,
      :kunde_seit, :info_interessent, :rating_bemerkung, :gevis_nummer, invoice_attributes: [ :partner_id, :firma1, :vorname, :nachname, :firma2, :abteilung, :titel, :geschlecht, :funktion, :strasse, :hausnummer, :plz, :ort, :postfach, :postfach_plz, :postfach_ort, :lastschriftart, :zahlungsart, :zahlungsziel],
      partner_role_attributes: [ :partner_id, :partnerrolle, :gueltig_von, :gueltig_bis ], contact_attributes: [ :partner_id, :ansprechpartner_vorname, :ansprechpartner_name, :ansprechpartner_titel, :ansprechpartner_abteilung, :ansprechpartner_geschlecht, :ansprechpartner_funktion, :ansprechpartner_telefon,
        :ansprechpartner_fax, :ansprechpartner_email])
    end
end
