json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :partner_id, :firma1, :vorname, :nachname, :firma2, :abteilung, :titel, :geschlecht, :funktion, :strasse, :hausnummer, :plz, :ort, :postfach, :postfach_plz, :postfach_ort, :lastschriftart, :zahlungsart, :zahlungsziel
  json.url invoice_url(invoice, format: :json)
end
