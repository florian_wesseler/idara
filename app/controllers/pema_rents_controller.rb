class PemaRentsController < ApplicationController
  before_action :set_pema_rent, only: [:show, :edit, :update, :destroy]

  # GET /pema_rents
  # GET /pema_rents.json
  def index
    @pema_rents = PemaRent.all
  end

  # GET /pema_rents/1
  # GET /pema_rents/1.json
  def show
  end

  # GET /pema_rents/new
  def new
    @pema_rent = PemaRent.new
  end

  # GET /pema_rents/1/edit
  def edit
  end

  # POST /pema_rents
  # POST /pema_rents.json
  def create
    @pema_rent = PemaRent.new(pema_rent_params)

    respond_to do |format|
      if @pema_rent.save
        format.html { redirect_to @pema_rent, notice: 'Pema rent was successfully created.' }
        format.json { render :show, status: :created, location: @pema_rent }
      else
        format.html { render :new }
        format.json { render json: @pema_rent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pema_rents/1
  # PATCH/PUT /pema_rents/1.json
  def update
    respond_to do |format|
      if @pema_rent.update(pema_rent_params)
        format.html { redirect_to @pema_rent, notice: 'Pema rent was successfully updated.' }
        format.json { render :show, status: :ok, location: @pema_rent }
      else
        format.html { render :edit }
        format.json { render json: @pema_rent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pema_rents/1
  # DELETE /pema_rents/1.json
  def destroy
    @pema_rent.destroy
    respond_to do |format|
      format.html { redirect_to pema_rents_url, notice: 'Pema rent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pema_rent
      @pema_rent = PemaRent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pema_rent_params
      params.require(:pema_rent).permit(:partner_id, :vertragsnummer, :fahrgestellnummer, :laufzeit, :kaution, :status, :vertragsbeginn, :vertragsende, :zusatzinfos)
    end
end
