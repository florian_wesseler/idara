class CreateReturnDebitNotes < ActiveRecord::Migration
  def change
    create_table :return_debit_notes do |t|
      t.references :partner, index: true, foreign_key: true
      t.datetime :eingang_rls
      t.string :grund
      t.datetime :faellig
      t.decimal :betrag
      t.datetime :ausgleich_rls
      t.decimal :rls_gebuehren
      t.string :tank_geschaeft
      t.datetime :dkv_informiert
      t.datetime :zahlungserinnerung
      t.datetime :mahnung1
      t.datetime :mahnung2
      t.datetime :zahlungseingang_datum
      t.decimal :zahlungseingang_betrag
      t.datetime :dkv_informiert2
      t.datetime :gueltig_bis

      t.timestamps null: false
    end
  end
end
