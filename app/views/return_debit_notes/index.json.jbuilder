json.array!(@return_debit_notes) do |return_debit_note|
  json.extract! return_debit_note, :id, :partner_id, :eingang_rls, :grund, :faellig, :betrag, :ausgleich_rls, :rls_gebuehren, :tank_geschaeft, :dkv_informiert, :zahlungserinnerung, :mahnung1, :mahnung2, :zahlungseingang_datum, :zahlungseingang_betrag, :dkv_informiert2, :gueltig_bis
  json.url return_debit_note_url(return_debit_note, format: :json)
end
