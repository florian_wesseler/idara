json.array!(@motor_pools) do |motor_pool|
  json.extract! motor_pool, :id, :partner_id, :anzahl_pkw_kl_3_5, :anzahl_pkw_gr_3_5, :anzahl_lkw_gr_12, :anzahl_buss, :tankstellen_privat, :mautabrechner, :hauptkartenabrechner, :erfassungsdatum
  json.url motor_pool_url(motor_pool, format: :json)
end
