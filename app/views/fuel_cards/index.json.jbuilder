json.array!(@fuel_cards) do |fuel_card|
  json.extract! fuel_card, :id, :partner_id, :dkv_nummer, :anzahl_, :gueltig_seit, :gueltig_bis
  json.url fuel_card_url(fuel_card, format: :json)
end
