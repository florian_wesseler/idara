require 'test_helper'

class FuelTurnoversControllerTest < ActionController::TestCase
  setup do
    @fuel_turnover = fuel_turnovers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fuel_turnovers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fuel_turnover" do
    assert_difference('FuelTurnover.count') do
      post :create, fuel_turnover: { belegdatum: @fuel_turnover.belegdatum, beschreibung: @fuel_turnover.beschreibung, betrag: @fuel_turnover.betrag, dkv_nummer: @fuel_turnover.dkv_nummer, partner_id: @fuel_turnover.partner_id, rechnungsdruck: @fuel_turnover.rechnungsdruck, waehrung: @fuel_turnover.waehrung }
    end

    assert_redirected_to fuel_turnover_path(assigns(:fuel_turnover))
  end

  test "should show fuel_turnover" do
    get :show, id: @fuel_turnover
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fuel_turnover
    assert_response :success
  end

  test "should update fuel_turnover" do
    patch :update, id: @fuel_turnover, fuel_turnover: { belegdatum: @fuel_turnover.belegdatum, beschreibung: @fuel_turnover.beschreibung, betrag: @fuel_turnover.betrag, dkv_nummer: @fuel_turnover.dkv_nummer, partner_id: @fuel_turnover.partner_id, rechnungsdruck: @fuel_turnover.rechnungsdruck, waehrung: @fuel_turnover.waehrung }
    assert_redirected_to fuel_turnover_path(assigns(:fuel_turnover))
  end

  test "should destroy fuel_turnover" do
    assert_difference('FuelTurnover.count', -1) do
      delete :destroy, id: @fuel_turnover
    end

    assert_redirected_to fuel_turnovers_path
  end
end
