class SalesInformationsController < ApplicationController
  before_action :set_sales_information, only: [:show, :edit, :update, :destroy]

  # GET /sales_informations
  # GET /sales_informations.json
  def index
    @sales_informations = SalesInformation.all
  end

  # GET /sales_informations/1
  # GET /sales_informations/1.json
  def show
  end

  # GET /sales_informations/new
  def new
    @sales_information = SalesInformation.new
  end

  # GET /sales_informations/1/edit
  def edit
  end

  # POST /sales_informations
  # POST /sales_informations.json
  def create
    @sales_information = SalesInformation.new(sales_information_params)

    respond_to do |format|
      if @sales_information.save
        format.html { redirect_to @sales_information, notice: 'Sales information was successfully created.' }
        format.json { render :show, status: :created, location: @sales_information }
      else
        format.html { render :new }
        format.json { render json: @sales_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_informations/1
  # PATCH/PUT /sales_informations/1.json
  def update
    respond_to do |format|
      if @sales_information.update(sales_information_params)
        format.html { redirect_to @sales_information, notice: 'Sales information was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_information }
      else
        format.html { render :edit }
        format.json { render json: @sales_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_informations/1
  # DELETE /sales_informations/1.json
  def destroy
    @sales_information.destroy
    respond_to do |format|
      format.html { redirect_to sales_informations_url, notice: 'Sales information was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_information
      @sales_information = SalesInformation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_information_params
      params.require(:sales_information).permit(:partner_id, :berater_vorname, :berater_name, :kompetenzcenter, :niederlassung, :gueltig_bis, :gueltig_von)
    end
end
