json.array!(@service_turnovers) do |service_turnover|
  json.extract! service_turnover, :id, :partner_id, :beschreibung, :belegdatum, :betrag, :waehrung, :rechnungsdruck, :gevis_akzeptanzstelle
  json.url service_turnover_url(service_turnover, format: :json)
end
