require 'test_helper'

class ReturnDebitNotesControllerTest < ActionController::TestCase
  setup do
    @return_debit_note = return_debit_notes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:return_debit_notes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create return_debit_note" do
    assert_difference('ReturnDebitNote.count') do
      post :create, return_debit_note: { ausgleich_rls: @return_debit_note.ausgleich_rls, betrag: @return_debit_note.betrag, dkv_informiert2: @return_debit_note.dkv_informiert2, dkv_informiert: @return_debit_note.dkv_informiert, eingang_rls: @return_debit_note.eingang_rls, faellig: @return_debit_note.faellig, grund: @return_debit_note.grund, gueltig_bis: @return_debit_note.gueltig_bis, mahnung1: @return_debit_note.mahnung1, mahnung2: @return_debit_note.mahnung2, partner_id: @return_debit_note.partner_id, rls_gebuehren: @return_debit_note.rls_gebuehren, tank_geschaeft: @return_debit_note.tank_geschaeft, zahlungseingang_betrag: @return_debit_note.zahlungseingang_betrag, zahlungseingang_datum: @return_debit_note.zahlungseingang_datum, zahlungserinnerung: @return_debit_note.zahlungserinnerung }
    end

    assert_redirected_to return_debit_note_path(assigns(:return_debit_note))
  end

  test "should show return_debit_note" do
    get :show, id: @return_debit_note
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @return_debit_note
    assert_response :success
  end

  test "should update return_debit_note" do
    patch :update, id: @return_debit_note, return_debit_note: { ausgleich_rls: @return_debit_note.ausgleich_rls, betrag: @return_debit_note.betrag, dkv_informiert2: @return_debit_note.dkv_informiert2, dkv_informiert: @return_debit_note.dkv_informiert, eingang_rls: @return_debit_note.eingang_rls, faellig: @return_debit_note.faellig, grund: @return_debit_note.grund, gueltig_bis: @return_debit_note.gueltig_bis, mahnung1: @return_debit_note.mahnung1, mahnung2: @return_debit_note.mahnung2, partner_id: @return_debit_note.partner_id, rls_gebuehren: @return_debit_note.rls_gebuehren, tank_geschaeft: @return_debit_note.tank_geschaeft, zahlungseingang_betrag: @return_debit_note.zahlungseingang_betrag, zahlungseingang_datum: @return_debit_note.zahlungseingang_datum, zahlungserinnerung: @return_debit_note.zahlungserinnerung }
    assert_redirected_to return_debit_note_path(assigns(:return_debit_note))
  end

  test "should destroy return_debit_note" do
    assert_difference('ReturnDebitNote.count', -1) do
      delete :destroy, id: @return_debit_note
    end

    assert_redirected_to return_debit_notes_path
  end
end
