class Partner < ActiveRecord::Base
  has_one :invoice
  accepts_nested_attributes_for :invoice

  has_one :contact
  accepts_nested_attributes_for :contact

  has_many :card_contracts
  has_many :fuel_cards
  has_many :fuel_limits
  has_many :fuel_turnovers
  has_many :liability_requests
  has_many :motor_pools
  has_many :partner_roles
  accepts_nested_attributes_for :partner_roles

  has_many :payment_troubles
  has_many :pema_rents
  has_many :return_debit_notes
  has_many :sales_informations
  has_many :service_limits
  has_many :service_turnovers
  has_many :terminations
  has_many :validations

end
