require 'test_helper'

class FuelCardsControllerTest < ActionController::TestCase
  setup do
    @fuel_card = fuel_cards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fuel_cards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fuel_card" do
    assert_difference('FuelCard.count') do
      post :create, fuel_card: { anzahl_: @fuel_card.anzahl_, dkv_nummer: @fuel_card.dkv_nummer, gueltig_bis: @fuel_card.gueltig_bis, gueltig_seit: @fuel_card.gueltig_seit, partner_id: @fuel_card.partner_id }
    end

    assert_redirected_to fuel_card_path(assigns(:fuel_card))
  end

  test "should show fuel_card" do
    get :show, id: @fuel_card
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fuel_card
    assert_response :success
  end

  test "should update fuel_card" do
    patch :update, id: @fuel_card, fuel_card: { anzahl_: @fuel_card.anzahl_, dkv_nummer: @fuel_card.dkv_nummer, gueltig_bis: @fuel_card.gueltig_bis, gueltig_seit: @fuel_card.gueltig_seit, partner_id: @fuel_card.partner_id }
    assert_redirected_to fuel_card_path(assigns(:fuel_card))
  end

  test "should destroy fuel_card" do
    assert_difference('FuelCard.count', -1) do
      delete :destroy, id: @fuel_card
    end

    assert_redirected_to fuel_cards_path
  end
end
