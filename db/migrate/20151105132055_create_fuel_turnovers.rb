class CreateFuelTurnovers < ActiveRecord::Migration
  def change
    create_table :fuel_turnovers do |t|
      t.integer :dkv_nummer
      t.references :partner, index: true, foreign_key: true
      t.string :beschreibung
      t.datetime :belegdatum
      t.decimal :betrag
      t.string :waehrung
      t.boolean :rechnungsdruck

      t.timestamps null: false
    end
  end
end
