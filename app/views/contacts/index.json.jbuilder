json.array!(@contacts) do |contact|
  json.extract! contact, :id, :partner_id, :ansprechpartner_vorname, :ansprechpartner_name, :ansprechpartner_titel, :ansprechpartner_abteilung, :ansprechpartner_geschlecht, :ansprechpartner_funktion, :ansprechpartner_telefon, :ansprechpartner_fax, :ansprechpartner_email
  json.url contact_url(contact, format: :json)
end
