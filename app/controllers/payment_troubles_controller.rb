class PaymentTroublesController < ApplicationController
  before_action :set_payment_trouble, only: [:show, :edit, :update, :destroy]

  # GET /payment_troubles
  # GET /payment_troubles.json
  def index
    @payment_troubles = PaymentTrouble.all
  end

  # GET /payment_troubles/1
  # GET /payment_troubles/1.json
  def show
  end

  # GET /payment_troubles/new
  def new
    @payment_trouble = PaymentTrouble.new
  end

  # GET /payment_troubles/1/edit
  def edit
  end

  # POST /payment_troubles
  # POST /payment_troubles.json
  def create
    @payment_trouble = PaymentTrouble.new(payment_trouble_params)

    respond_to do |format|
      if @payment_trouble.save
        format.html { redirect_to @payment_trouble, notice: 'Payment trouble was successfully created.' }
        format.json { render :show, status: :created, location: @payment_trouble }
      else
        format.html { render :new }
        format.json { render json: @payment_trouble.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payment_troubles/1
  # PATCH/PUT /payment_troubles/1.json
  def update
    respond_to do |format|
      if @payment_trouble.update(payment_trouble_params)
        format.html { redirect_to @payment_trouble, notice: 'Payment trouble was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment_trouble }
      else
        format.html { render :edit }
        format.json { render json: @payment_trouble.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payment_troubles/1
  # DELETE /payment_troubles/1.json
  def destroy
    @payment_trouble.destroy
    respond_to do |format|
      format.html { redirect_to payment_troubles_url, notice: 'Payment trouble was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_trouble
      @payment_trouble = PaymentTrouble.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_trouble_params
      params.require(:payment_trouble).permit(:partner_id, :watchlist, :watchlist_datum, :mahnstufe, :mahnstufe_datum, :sperrgrund, :sperrgrund_datum)
    end
end
