json.array!(@fuel_limits) do |fuel_limit|
  json.extract! fuel_limit, :id, :partner_id, :dkv_nummer, :limit, :limit_seit, :limit_bis, :haftungszusage_dkv_seit
  json.url fuel_limit_url(fuel_limit, format: :json)
end
