json.array!(@pema_rents) do |pema_rent|
  json.extract! pema_rent, :id, :partner_id, :vertragsnummer, :fahrgestellnummer, :laufzeit, :kaution, :status, :vertragsbeginn, :vertragsende, :zusatzinfos
  json.url pema_rent_url(pema_rent, format: :json)
end
