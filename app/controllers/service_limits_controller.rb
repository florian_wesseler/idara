class ServiceLimitsController < ApplicationController
  before_action :set_service_limit, only: [:show, :edit, :update, :destroy]

  # GET /service_limits
  # GET /service_limits.json
  def index
    @service_limits = ServiceLimit.all
  end

  # GET /service_limits/1
  # GET /service_limits/1.json
  def show
  end

  # GET /service_limits/new
  def new
    @service_limit = ServiceLimit.new
  end

  # GET /service_limits/1/edit
  def edit
  end

  # POST /service_limits
  # POST /service_limits.json
  def create
    @service_limit = ServiceLimit.new(service_limit_params)

    respond_to do |format|
      if @service_limit.save
        format.html { redirect_to @service_limit, notice: 'Service limit was successfully created.' }
        format.json { render :show, status: :created, location: @service_limit }
      else
        format.html { render :new }
        format.json { render json: @service_limit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_limits/1
  # PATCH/PUT /service_limits/1.json
  def update
    respond_to do |format|
      if @service_limit.update(service_limit_params)
        format.html { redirect_to @service_limit, notice: 'Service limit was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_limit }
      else
        format.html { render :edit }
        format.json { render json: @service_limit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_limits/1
  # DELETE /service_limits/1.json
  def destroy
    @service_limit.destroy
    respond_to do |format|
      format.html { redirect_to service_limits_url, notice: 'Service limit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_limit
      @service_limit = ServiceLimit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_limit_params
      params.require(:service_limit).permit(:partner_id, :limit, :limit_seit, :limit_bis, :service_seit)
    end
end
