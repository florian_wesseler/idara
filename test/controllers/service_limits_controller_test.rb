require 'test_helper'

class ServiceLimitsControllerTest < ActionController::TestCase
  setup do
    @service_limit = service_limits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_limits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_limit" do
    assert_difference('ServiceLimit.count') do
      post :create, service_limit: { limit: @service_limit.limit, limit_bis: @service_limit.limit_bis, limit_seit: @service_limit.limit_seit, partner_id: @service_limit.partner_id, service_seit: @service_limit.service_seit }
    end

    assert_redirected_to service_limit_path(assigns(:service_limit))
  end

  test "should show service_limit" do
    get :show, id: @service_limit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_limit
    assert_response :success
  end

  test "should update service_limit" do
    patch :update, id: @service_limit, service_limit: { limit: @service_limit.limit, limit_bis: @service_limit.limit_bis, limit_seit: @service_limit.limit_seit, partner_id: @service_limit.partner_id, service_seit: @service_limit.service_seit }
    assert_redirected_to service_limit_path(assigns(:service_limit))
  end

  test "should destroy service_limit" do
    assert_difference('ServiceLimit.count', -1) do
      delete :destroy, id: @service_limit
    end

    assert_redirected_to service_limits_path
  end
end
