class CardContractsController < ApplicationController
  before_action :set_card_contract, only: [:show, :edit, :update, :destroy]

  # GET /card_contracts
  # GET /card_contracts.json
  def index
    @card_contracts = CardContract.all
  end

  # GET /card_contracts/1
  # GET /card_contracts/1.json
  def show
  end

  # GET /card_contracts/new
  def new
    @card_contract = CardContract.new
  end

  # GET /card_contracts/1/edit
  def edit
  end

  # POST /card_contracts
  # POST /card_contracts.json
  def create
    @card_contract = CardContract.new(card_contract_params)

    respond_to do |format|
      if @card_contract.save
        format.html { redirect_to @card_contract, notice: 'Card contract was successfully created.' }
        format.json { render :show, status: :created, location: @card_contract }
      else
        format.html { render :new }
        format.json { render json: @card_contract.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /card_contracts/1
  # PATCH/PUT /card_contracts/1.json
  def update
    respond_to do |format|
      if @card_contract.update(card_contract_params)
        format.html { redirect_to @card_contract, notice: 'Card contract was successfully updated.' }
        format.json { render :show, status: :ok, location: @card_contract }
      else
        format.html { render :edit }
        format.json { render json: @card_contract.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /card_contracts/1
  # DELETE /card_contracts/1.json
  def destroy
    @card_contract.destroy
    respond_to do |format|
      format.html { redirect_to card_contracts_url, notice: 'Card contract was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card_contract
      @card_contract = CardContract.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def card_contract_params
      params.require(:card_contract).permit(:partner_id, :kundentyp, :vertragsnummer, :vertrag_datum, :vermerk)
    end
end
