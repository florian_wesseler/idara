require 'test_helper'

class PaymentTroublesControllerTest < ActionController::TestCase
  setup do
    @payment_trouble = payment_troubles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payment_troubles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payment_trouble" do
    assert_difference('PaymentTrouble.count') do
      post :create, payment_trouble: { mahnstufe: @payment_trouble.mahnstufe, mahnstufe_datum: @payment_trouble.mahnstufe_datum, partner_id: @payment_trouble.partner_id, sperrgrund: @payment_trouble.sperrgrund, sperrgrund_datum: @payment_trouble.sperrgrund_datum, watchlist: @payment_trouble.watchlist, watchlist_datum: @payment_trouble.watchlist_datum }
    end

    assert_redirected_to payment_trouble_path(assigns(:payment_trouble))
  end

  test "should show payment_trouble" do
    get :show, id: @payment_trouble
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payment_trouble
    assert_response :success
  end

  test "should update payment_trouble" do
    patch :update, id: @payment_trouble, payment_trouble: { mahnstufe: @payment_trouble.mahnstufe, mahnstufe_datum: @payment_trouble.mahnstufe_datum, partner_id: @payment_trouble.partner_id, sperrgrund: @payment_trouble.sperrgrund, sperrgrund_datum: @payment_trouble.sperrgrund_datum, watchlist: @payment_trouble.watchlist, watchlist_datum: @payment_trouble.watchlist_datum }
    assert_redirected_to payment_trouble_path(assigns(:payment_trouble))
  end

  test "should destroy payment_trouble" do
    assert_difference('PaymentTrouble.count', -1) do
      delete :destroy, id: @payment_trouble
    end

    assert_redirected_to payment_troubles_path
  end
end
