json.array!(@liability_requests) do |liability_request|
  json.extract! liability_request, :id, :partner_id, :anfrage_dkv, :dkv_betrag, :anfrage_verschickt_an, :anfrage_verschickt_datum, :ergebnis_dkv_haftungsanfrage, :anzahl_karten, :mitarbeiter_ad, :mitarbeiter_ad_datum
  json.url liability_request_url(liability_request, format: :json)
end
