class CreateTerminations < ActiveRecord::Migration
  def change
    create_table :terminations do |t|
      t.references :partner, index: true, foreign_key: true
      t.integer :variante
      t.string :sofort
      t.datetime :terminiert
      t.string :status

      t.timestamps null: false
    end
  end
end
