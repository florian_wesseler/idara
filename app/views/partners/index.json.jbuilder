json.array!(@partners) do |partner|
  json.extract! partner, :id, :partner_typ, :dkv_nummer, :bemerkungen, :abteilung, :plz_postfach, :ort_postfach, :postfach, :handelsregister_ort, :handelsregister_nr, :kunde_seit, :info_interessent, :rating_bemerkung
  json.url partner_url(partner, format: :json)
end
