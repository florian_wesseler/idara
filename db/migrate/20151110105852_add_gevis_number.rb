class AddGevisNumber < ActiveRecord::Migration
  def self.up
    add_column :partners, :gevis_nummer, :integer, presence: true, null: false
  end

  def self.down
    remove_column :partners, :gevis_nummer
  end

end
