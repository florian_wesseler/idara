require 'test_helper'

class CardContractsControllerTest < ActionController::TestCase
  setup do
    @card_contract = card_contracts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:card_contracts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create card_contract" do
    assert_difference('CardContract.count') do
      post :create, card_contract: { kundentyp: @card_contract.kundentyp, partner_id: @card_contract.partner_id, vermerk: @card_contract.vermerk, vertrag_datum: @card_contract.vertrag_datum, vertragsnummer: @card_contract.vertragsnummer }
    end

    assert_redirected_to card_contract_path(assigns(:card_contract))
  end

  test "should show card_contract" do
    get :show, id: @card_contract
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @card_contract
    assert_response :success
  end

  test "should update card_contract" do
    patch :update, id: @card_contract, card_contract: { kundentyp: @card_contract.kundentyp, partner_id: @card_contract.partner_id, vermerk: @card_contract.vermerk, vertrag_datum: @card_contract.vertrag_datum, vertragsnummer: @card_contract.vertragsnummer }
    assert_redirected_to card_contract_path(assigns(:card_contract))
  end

  test "should destroy card_contract" do
    assert_difference('CardContract.count', -1) do
      delete :destroy, id: @card_contract
    end

    assert_redirected_to card_contracts_path
  end
end
