class CreateFuelCards < ActiveRecord::Migration
  def change
    create_table :fuel_cards do |t|
      t.references :partner, index: true, foreign_key: true
      t.integer :dkv_nummer
      t.integer :anzahl_
      t.datetime :gueltig_seit
      t.datetime :gueltig_bis

      t.timestamps null: false
    end
  end
end
