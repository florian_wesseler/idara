json.array!(@partner_roles) do |partner_role|
  json.extract! partner_role, :id, :partner_id, :partnerrolle, :gueltig_von, :gueltig_bis
  json.url partner_role_url(partner_role, format: :json)
end
