json.array!(@card_contracts) do |card_contract|
  json.extract! card_contract, :id, :partner_id, :kundentyp, :vertragsnummer, :vertrag_datum, :vermerk
  json.url card_contract_url(card_contract, format: :json)
end
