json.array!(@sales_informations) do |sales_information|
  json.extract! sales_information, :id, :partner_id, :berater_vorname, :berater_name, :kompetenzcenter, :niederlassung, :gueltig_bis, :gueltig_von
  json.url sales_information_url(sales_information, format: :json)
end
