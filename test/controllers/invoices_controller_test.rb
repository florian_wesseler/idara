require 'test_helper'

class InvoicesControllerTest < ActionController::TestCase
  setup do
    @invoice = invoices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:invoices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create invoice" do
    assert_difference('Invoice.count') do
      post :create, invoice: { abteilung: @invoice.abteilung, firma1: @invoice.firma1, firma2: @invoice.firma2, funktion: @invoice.funktion, geschlecht: @invoice.geschlecht, hausnummer: @invoice.hausnummer, lastschriftart: @invoice.lastschriftart, nachname: @invoice.nachname, ort: @invoice.ort, partner_id: @invoice.partner_id, plz: @invoice.plz, postfach: @invoice.postfach, postfach_ort: @invoice.postfach_ort, postfach_plz: @invoice.postfach_plz, strasse: @invoice.strasse, titel: @invoice.titel, vorname: @invoice.vorname, zahlungsart: @invoice.zahlungsart, zahlungsziel: @invoice.zahlungsziel }
    end

    assert_redirected_to invoice_path(assigns(:invoice))
  end

  test "should show invoice" do
    get :show, id: @invoice
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @invoice
    assert_response :success
  end

  test "should update invoice" do
    patch :update, id: @invoice, invoice: { abteilung: @invoice.abteilung, firma1: @invoice.firma1, firma2: @invoice.firma2, funktion: @invoice.funktion, geschlecht: @invoice.geschlecht, hausnummer: @invoice.hausnummer, lastschriftart: @invoice.lastschriftart, nachname: @invoice.nachname, ort: @invoice.ort, partner_id: @invoice.partner_id, plz: @invoice.plz, postfach: @invoice.postfach, postfach_ort: @invoice.postfach_ort, postfach_plz: @invoice.postfach_plz, strasse: @invoice.strasse, titel: @invoice.titel, vorname: @invoice.vorname, zahlungsart: @invoice.zahlungsart, zahlungsziel: @invoice.zahlungsziel }
    assert_redirected_to invoice_path(assigns(:invoice))
  end

  test "should destroy invoice" do
    assert_difference('Invoice.count', -1) do
      delete :destroy, id: @invoice
    end

    assert_redirected_to invoices_path
  end
end
