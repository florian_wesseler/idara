json.array!(@payment_troubles) do |payment_trouble|
  json.extract! payment_trouble, :id, :partner_id, :watchlist, :watchlist_datum, :mahnstufe, :mahnstufe_datum, :sperrgrund, :sperrgrund_datum
  json.url payment_trouble_url(payment_trouble, format: :json)
end
