class CreateCardContracts < ActiveRecord::Migration
  def change
    create_table :card_contracts do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :kundentyp
      t.integer :vertragsnummer
      t.datetime :vertrag_datum
      t.string :vermerk

      t.timestamps null: false
    end
  end
end
