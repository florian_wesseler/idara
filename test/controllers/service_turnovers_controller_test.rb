require 'test_helper'

class ServiceTurnoversControllerTest < ActionController::TestCase
  setup do
    @service_turnover = service_turnovers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_turnovers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_turnover" do
    assert_difference('ServiceTurnover.count') do
      post :create, service_turnover: { belegdatum: @service_turnover.belegdatum, beschreibung: @service_turnover.beschreibung, betrag: @service_turnover.betrag, gevis_akzeptanzstelle: @service_turnover.gevis_akzeptanzstelle, partner_id: @service_turnover.partner_id, rechnungsdruck: @service_turnover.rechnungsdruck, waehrung: @service_turnover.waehrung }
    end

    assert_redirected_to service_turnover_path(assigns(:service_turnover))
  end

  test "should show service_turnover" do
    get :show, id: @service_turnover
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_turnover
    assert_response :success
  end

  test "should update service_turnover" do
    patch :update, id: @service_turnover, service_turnover: { belegdatum: @service_turnover.belegdatum, beschreibung: @service_turnover.beschreibung, betrag: @service_turnover.betrag, gevis_akzeptanzstelle: @service_turnover.gevis_akzeptanzstelle, partner_id: @service_turnover.partner_id, rechnungsdruck: @service_turnover.rechnungsdruck, waehrung: @service_turnover.waehrung }
    assert_redirected_to service_turnover_path(assigns(:service_turnover))
  end

  test "should destroy service_turnover" do
    assert_difference('ServiceTurnover.count', -1) do
      delete :destroy, id: @service_turnover
    end

    assert_redirected_to service_turnovers_path
  end
end
