class CreatePaymentTroubles < ActiveRecord::Migration
  def change
    create_table :payment_troubles do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :watchlist
      t.datetime :watchlist_datum
      t.integer :mahnstufe
      t.datetime :mahnstufe_datum
      t.string :sperrgrund
      t.datetime :sperrgrund_datum

      t.timestamps null: false
    end
  end
end
