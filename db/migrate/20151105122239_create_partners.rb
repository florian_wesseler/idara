class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :partner_typ
      t.integer :dkv_nummer
      t.string :bemerkungen
      t.string :abteilung
      t.integer :plz_postfach
      t.string :ort_postfach
      t.string :postfach
      t.string :handelsregister_ort
      t.integer :handelsregister_nr
      t.datetime :kunde_seit
      t.string :info_interessent
      t.string :rating_bemerkung

      t.timestamps null: false
    end
  end
end
