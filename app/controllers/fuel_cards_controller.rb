class FuelCardsController < ApplicationController
  before_action :set_fuel_card, only: [:show, :edit, :update, :destroy]

  # GET /fuel_cards
  # GET /fuel_cards.json
  def index
    @fuel_cards = FuelCard.all
  end

  # GET /fuel_cards/1
  # GET /fuel_cards/1.json
  def show
  end

  # GET /fuel_cards/new
  def new
    @fuel_card = FuelCard.new
  end

  # GET /fuel_cards/1/edit
  def edit
  end

  # POST /fuel_cards
  # POST /fuel_cards.json
  def create
    @fuel_card = FuelCard.new(fuel_card_params)

    respond_to do |format|
      if @fuel_card.save
        format.html { redirect_to @fuel_card, notice: 'Fuel card was successfully created.' }
        format.json { render :show, status: :created, location: @fuel_card }
      else
        format.html { render :new }
        format.json { render json: @fuel_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fuel_cards/1
  # PATCH/PUT /fuel_cards/1.json
  def update
    respond_to do |format|
      if @fuel_card.update(fuel_card_params)
        format.html { redirect_to @fuel_card, notice: 'Fuel card was successfully updated.' }
        format.json { render :show, status: :ok, location: @fuel_card }
      else
        format.html { render :edit }
        format.json { render json: @fuel_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fuel_cards/1
  # DELETE /fuel_cards/1.json
  def destroy
    @fuel_card.destroy
    respond_to do |format|
      format.html { redirect_to fuel_cards_url, notice: 'Fuel card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fuel_card
      @fuel_card = FuelCard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fuel_card_params
      params.require(:fuel_card).permit(:partner_id, :dkv_nummer, :anzahl_, :gueltig_seit, :gueltig_bis)
    end
end
