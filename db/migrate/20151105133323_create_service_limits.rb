class CreateServiceLimits < ActiveRecord::Migration
  def change
    create_table :service_limits do |t|
      t.references :partner, index: true, foreign_key: true
      t.decimal :limit
      t.datetime :limit_seit
      t.datetime :limit_bis
      t.datetime :service_seit

      t.timestamps null: false
    end
  end
end
