json.array!(@service_limits) do |service_limit|
  json.extract! service_limit, :id, :partner_id, :limit, :limit_seit, :limit_bis, :service_seit
  json.url service_limit_url(service_limit, format: :json)
end
