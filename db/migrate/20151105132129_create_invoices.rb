class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :firma1
      t.string :vorname
      t.string :nachname
      t.string :firma2
      t.string :abteilung
      t.string :titel
      t.string :geschlecht
      t.string :funktion
      t.string :strasse
      t.string :hausnummer
      t.integer :plz
      t.string :ort
      t.string :postfach
      t.integer :postfach_plz
      t.string :postfach_ort
      t.string :lastschriftart
      t.string :zahlungsart
      t.integer :zahlungsziel

      t.timestamps null: false
    end
  end
end
