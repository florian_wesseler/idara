class CreatePartnerRoles < ActiveRecord::Migration
  def change
    create_table :partner_roles do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :partnerrolle
      t.datetime :gueltig_von
      t.datetime :gueltig_bis

      t.timestamps null: false
    end
  end
end
