require 'test_helper'

class ContactsControllerTest < ActionController::TestCase
  setup do
    @contact = contacts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contacts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contact" do
    assert_difference('Contact.count') do
      post :create, contact: { ansprechpartner_abteilung: @contact.ansprechpartner_abteilung, ansprechpartner_email: @contact.ansprechpartner_email, ansprechpartner_fax: @contact.ansprechpartner_fax, ansprechpartner_funktion: @contact.ansprechpartner_funktion, ansprechpartner_geschlecht: @contact.ansprechpartner_geschlecht, ansprechpartner_name: @contact.ansprechpartner_name, ansprechpartner_telefon: @contact.ansprechpartner_telefon, ansprechpartner_titel: @contact.ansprechpartner_titel, ansprechpartner_vorname: @contact.ansprechpartner_vorname, partner_id: @contact.partner_id }
    end

    assert_redirected_to contact_path(assigns(:contact))
  end

  test "should show contact" do
    get :show, id: @contact
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contact
    assert_response :success
  end

  test "should update contact" do
    patch :update, id: @contact, contact: { ansprechpartner_abteilung: @contact.ansprechpartner_abteilung, ansprechpartner_email: @contact.ansprechpartner_email, ansprechpartner_fax: @contact.ansprechpartner_fax, ansprechpartner_funktion: @contact.ansprechpartner_funktion, ansprechpartner_geschlecht: @contact.ansprechpartner_geschlecht, ansprechpartner_name: @contact.ansprechpartner_name, ansprechpartner_telefon: @contact.ansprechpartner_telefon, ansprechpartner_titel: @contact.ansprechpartner_titel, ansprechpartner_vorname: @contact.ansprechpartner_vorname, partner_id: @contact.partner_id }
    assert_redirected_to contact_path(assigns(:contact))
  end

  test "should destroy contact" do
    assert_difference('Contact.count', -1) do
      delete :destroy, id: @contact
    end

    assert_redirected_to contacts_path
  end
end
