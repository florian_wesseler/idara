class CreateSalesInformations < ActiveRecord::Migration
  def change
    create_table :sales_informations do |t|
      t.references :partner, index: true, foreign_key: true
      t.string :berater_vorname
      t.string :berater_name
      t.string :kompetenzcenter
      t.string :niederlassung
      t.datetime :gueltig_bis
      t.datetime :gueltig_von

      t.timestamps null: false
    end
  end
end
